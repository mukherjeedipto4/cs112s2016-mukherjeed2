import java.util.Scanner;
import java.util.Random;
public class RPS {
	static 	int userScore = 0, computerScore = 0 ,tiesScore = 0;
	public static void main (String [] args) {

		do {
			playRound();

		}while(playAgain());

		System.out.println("Thanks for playing!");

	
	}//main

	public static int roundWinner (String u, String c) {

		//0 = user wins
		//1 = computer wins
		//2 = tie

		if(u.equals(c)) {
			return 2;
		}
		else if(u.equals("ROCK") && c.equals("PAPER")) {
			return 1;
		}
		else if(u.equals("PAPER") && c.equals("ROCK")) {
			return 0;
		}
		else if(u.equals("ROCK") && c.equals("SCISSORS")) {
			return 0;
		}
		else if(u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;
		}
		else if(u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public static String generateComputerChoice() {
		//0 = ROCK
		//1 = PAPER
		//2 = SCISSORS

		Random rand = new Random();
		int computerChoice;

		computerChoice = rand.nextInt(3);

		if(computerChoice == 0) {
			System.out.println("Computer picks Rock");
			return "ROCK";
		}
		else if (computerChoice == 1) {
			System.out.println("Computer picks Paper");
			return "PAPER";
		}
		
		else if(computerChoice == 2) {
			System.out.println("Computer picks Scissors");
			return "SCISSORS";
		}//if-else

		System.out.println("Returning default : ROCK");
		return "ROCK";

	}

	public static String getUserInput() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your choice (ROCK,PAPER,SCISSORS) : ");

		String userChoice = scan.next().toUpperCase();


		return userChoice;
	}
	public static boolean playAgain() {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Play again? (YES,NO)");
		String userInput = scan.next().toUpperCase();

		if(userInput.equals("YES")) {
			return true;
		}
		else if(userInput.equals("NO")){
			return false;
		}
		else {
			System.out.println("Command unknown!");
			return playAgain();
		}

	}

	public static void playRound() {
		String userChoice,computerChoice;
	
		int winner;
		//Get a command from user
		userChoice = getUserInput();
		//Generate a computer choice
		computerChoice = generateComputerChoice();
		winner = roundWinner(userChoice,computerChoice);

		//Increment the appropriate score
		switch(winner) {
			case 0 : 
				userScore++;
				break;
			case 1 :
				computerScore++;
				break;
			case 2 :
				tiesScore++;
				break;
			default :
				System.out.println("Something went wrong!");
				break;
		}

		//Display the scores
		printScores(userScore,computerScore,tiesScore);

	}
	public static void printScores(int u,int c,int t) {
		System.out.printf("User score :  " + u );
		System.out.println("Computer score : "+c);
		System.out.println("Ties score : " + t);


	}

}
