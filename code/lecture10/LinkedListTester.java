import java.util.LinkedList;
import java.util.Iterator;
public class LinkedListTester {
	public static void main (String [] args) {

		LinkedList myList = new LinkedList();

		//add stuff to list
		myList.add(12);
		myList.add(99);
		myList.add(37);
		myList.add("Dank meme");

		printListContents(myList);
		//retrieve something from list
		//System.out.println(myList.get(0));
		//
		myList.remove(1);
		printListContents(myList);

		myList.add(1,99);

		printListContents(myList);

		LinkedList<Double> myList2 = new LinkedList<Double>();
		myList2.add(123.456);
		myList2.add(412.814);
		myList2.add(7.0);

		printListContents(myList2);

	}

	public static void printListContents(LinkedList ll) {
		/*for(int i =0;i<ll.size() ; i++) {
		  System.out.println(ll.get(i));
		  }*/

		Iterator iter = ll.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

		System.out.println();
	}



}

