class ArrayQueue {

	int [] myData;
	int front,back,size;

	public ArrayQueue(int n) {
		myData = new int[n];
		front =0;
		back = 0;
		size = 0;
	}

	public void add(int data) {
		if(size == myData.length) {
			System.out.println("queue is full");
			return;
		}
		myData[back] = data;
		back++;
		size++;

		if(back==myData.length) {
			back = 0;
		}
		
	}

	public int remove() {
		if(isEmpty()) {
			System.out.println("empty queue");
			return -1;
		} 
		int temp = myData[front];
		myData[front] = 0;
		front++;
		size--;
		if(front == myData.length) {
			front = 0;
		}
		
		return temp;
	}

	public int element() {
		if(isEmpty()) {
			System.out.println("error : empty queue");
			return -1;
		} else {
			return myData[front];
		}
	}

	public int size() {
		return size;

	}

	public boolean isEmpty() {
		if(size == 0)
			return true;
		else 
			return false;
	}

}
