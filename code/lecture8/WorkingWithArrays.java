public class WorkingWithArrays {
	public static void main(String [] args) {
		double [] a = {3,6.5,2,-6,19,0,3};

		double sum = 0;
		int minLoc = 0;
		int maxLoc = 0;
		int counter = 0;

		for(double current : a) {
			sum = sum + current;
		
			if(current > a[maxLoc]) {
				maxLoc = counter;
			}
			
			if(current < a[minLoc]) {
				minLoc = counter;
			}

			counter++;
		}

		System.out.println("Our average is: " + (sum/a.length));
		System.out.println("Min loc : "+minLoc);
		System.out.println("Max loc : "+maxLoc);

	}

}

