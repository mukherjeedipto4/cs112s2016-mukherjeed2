public class Player {
	private int score;

	public Player() {
		score = 0;
	}

	public Player(int startingScore) {
		score = startingScore;
	}

	public int getScore() {
		return score;
	}

	public void incrementScore() {
		score++;
	}

}
