import java.util.Random;
public class Computer extends Player{

	//private int computerScore;

	public Computer() {
		super(0);
	//	computerScore = 0;
	}
	
/*	public int getComputerScore() {
		return computerScore;
	}
*/
	public String generateComputerChoice() {
		//0 = ROCK
		//1 = PAPER
		//2 = SCISSORS

		Random rand = new Random();
		int computerChoice;

		computerChoice = rand.nextInt(3);

		if(computerChoice == 0) {
			System.out.println("Computer picks Rock");
			return "ROCK";
		}
		else if (computerChoice == 1) {
			System.out.println("Computer picks Paper");
			return "PAPER";
		}

		else if(computerChoice == 2) {
			System.out.println("Computer picks Scissors");
			return "SCISSORS";
		}//if-else

		System.out.println("Returning default : ROCK");
		return "ROCK";

	}

/*	public void incrementScore() {
		computerScore++;
	}
*/


}
