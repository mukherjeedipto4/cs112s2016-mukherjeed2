import java.util.Scanner;
public class Game {
	private static int tiesScore;
	private static Human player;
	private static Computer cpu;

	public static void main (String [] args) {
		player = new Human();
		cpu = new Computer();
		do {
			playRound();

		}while(playAgain());

		System.out.println("Thanks for playing!");


	}//main

	public static int roundWinner (String u, String c) {

		//0 = user wins
		//1 = computer wins
		//2 = tie

		if(u.equals(c)) {
			return 2;
		}
		else if(u.equals("ROCK") && c.equals("PAPER")) {
			return 1;
		}
		else if(u.equals("PAPER") && c.equals("ROCK")) {
			return 0;
		}
		else if(u.equals("ROCK") && c.equals("SCISSORS")) {
			return 0;
		}
		else if(u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;
		}
		else if(u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;
		}
		else {
			return 0;
		}
	}
	public static boolean playAgain() {

		Scanner scan = new Scanner(System.in);
		System.out.println("Play again? (YES,NO)");
		String userInput = scan.next().toUpperCase();

		if(userInput.equals("YES")) {
			return true;
		}
		else if(userInput.equals("NO")){
			return false;
		}
		else {
			System.out.println("Command unknown!");
			return playAgain();
		}

	}

	public static void playRound() {
		String userChoice,computerChoice;
		
		int winner;
		//Get a command from user
		userChoice = player.getUserInput();
		//Generate a computer choice
		computerChoice = cpu.generateComputerChoice();
		winner = roundWinner(userChoice,computerChoice);

		//Increment the appropriate score
		switch(winner) {
			case 0 : 
				player.incrementScore();
				break;
			case 1 :
				cpu.incrementScore();
				break;
			case 2 :
				tiesScore++;
				break;
			default :
				System.out.println("Something went wrong!");
				break;
		}

		//Display the scores
		printScores(player.getUserScore(),cpu.getComputerScore(),tiesScore);

	}
	public static void printScores(int u,int c,int t) {
		System.out.println("User score :  " + u );
		System.out.println("Computer score : "+c);
		System.out.println("Ties score : " + t);


	}




}
