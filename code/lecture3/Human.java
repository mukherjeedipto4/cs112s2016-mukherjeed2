import java.util.Scanner;
public class Human {

	private int userScore;

	public Human() {
		userScore = 0;
	}

	public int getUserScore() {
		return userScore;
	}

	public String getUserInput() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your choice (ROCK,PAPER,SCISSORS) : ");

		String userChoice = scan.next().toUpperCase();


		return userChoice;
	}

	public void incrementScore() {
		userScore++;
	}


}
