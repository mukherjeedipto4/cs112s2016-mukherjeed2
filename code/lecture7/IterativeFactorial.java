import java.util.Scanner;
public class IterativeFactorial {
	public static void main(String [] args) {
	
		System.out.println("Enter a number:");
		Scanner scan = new Scanner(System.in);
		int target = scan.nextInt();

		int product = 1;

		for(int i =target;i>1;i--) {
			product = product*i;
		}

		System.out.println(target + "! is " + product);
	
	}

}

