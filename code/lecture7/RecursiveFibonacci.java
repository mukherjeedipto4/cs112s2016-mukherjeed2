import java.util.Scanner;
public class RecursiveFibonacci {
	public static void main(String [] args) {

		System.out.println("Enter a number:");
		Scanner scan = new Scanner(System.in);
		int target = scan.nextInt();

		int output = fib(target);

		System.out.println("Fibonacci number "+target + " is " + output);

	}

	public static int fib(int n) {

		//base case
		if (n == 0)
			return 0;
		else if (n==1)
			return 1;
		//recursive case
		else {
			return fib(n-1)+fib(n-2);
		}
	}
}


