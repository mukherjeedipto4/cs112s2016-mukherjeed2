import java.util.Scanner;
public class RecursiveFactorial {
	public static void main(String [] args) {

		System.out.println("Enter a number:");
		Scanner scan = new Scanner(System.in);
		int target = scan.nextInt();

		int product = fact(target);

		System.out.println(target + "! is " + product);

	}

	public static int fact(int n) {
		
		//base case
		if(n == 1) {
			return 1;
		}
		//recursive case
		else {
			return fact(n-1) * n;
		}
	}

}
