public class TwoDimensionalArray {
	public static void main(String []  args) {
		int myArray[][] = {
			{7,12,4,-1},
			{-2,3},
			{8,9,0,1},
			{-12,3,4},
			{2}
		};
		int count = 0;
		int maxNum = myArray[0][0];
		int minNum = myArray[0][0];
		String maxIndex = new String();
		String minIndex = new String();
		double sum = 0;
		for(int i =0;i<myArray.length;i++) {
			for(int j=0;j<myArray[i].length;j++) {
				if(myArray[i][j]>maxNum) {
					maxNum = myArray[i][j];
					maxIndex = "["+i+"]["+j+"]";
				}
				if(myArray[i][j]<minNum) {
					minNum = myArray[i][j];
					minIndex = "["+i+"]["+j+"]";
				}
				sum+=myArray[i][j];
				count++;
			}
		}
		System.out.println("Average is : "+(sum/count));
		System.out.println("Max loc : "+maxIndex);
		System.out.println("Min loc : "+minIndex);
	}
}



