class LLStack<KEY> {

	private LL<KEY> internalList;

	public LLStack() {
		internalList = new LL<KEY>();
	}
	public void push(KEY data) {
		internalList.add(0,data);
	}

	public KEY peek() {
		if(empty()) {
			System.out.println("empty stack!");
			return null;
		} else {
			return internalList.get(0);
		}
	}

	public KEY pop() {
		if(empty()) {
			System.out.println("empty stack!");
			return null;
		}
		else {
			KEY data = peek();
			internalList.remove(0);
			return data;
		}
	}

	public int size() {
		return internalList.size();
	}

	public boolean empty() {
		if(internalList.size() == 0)
			return true;
		else
			return false;

	}

}
