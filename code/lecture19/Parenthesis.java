//import java.util.Stack;
class Parenthesis {
	public static void main(String [] args) {
		char input[] = args[0].toCharArray();

		if(matcher(input)) {
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		}

	}

	public static boolean matcher(char [] c) {
		LLStack<Character> myStack = new LLStack<Character>();
		for(int i =0;i<c.length;i++) {
			if(c[i] == '(' || c[i] == '[' || c[i] == '{') {
				myStack.push(c[i]);
			}
			else if(!myStack.empty()) {
				if(c[i] == ')'){
					char temp = myStack.pop();
					if(temp != '(' ) {
						return false;
					}
				}
				else if(c[i] == ']') {
					char temp = myStack.pop();
					if(temp != '[') {
						return false;
					}

				}
				else if(c[i] == '}') {
					char temp = myStack.pop();
					if(temp != '{') {
						return false;
					}

				}
			} else if(myStack.empty() && (c[i] ==')' || c[i] == '}' || c[i] == ']')){
				return false;
			}
				
		}
		if(myStack.size() != 0) {
			return false;
		} else {
			return true;
		}
	}
}
