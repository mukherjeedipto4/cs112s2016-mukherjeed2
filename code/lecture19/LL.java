public class LL<KEY> {

	private class Node<KEY> {
		protected Node<KEY> next;
		protected KEY data;

		public Node() {
			next = null;
			data = null;
		}

		public Node(KEY d, Node<KEY> n) {
			next = n;
			data = d;
		}

	}
	//LL stuff starts here
	private int size;
	private Node<KEY> head , tail;

	public LL() {
		head = null;
		tail = null;
		size = 0;

	}

	public void add(KEY d) {
		Node<KEY> newNode = new Node<KEY>(d,null); //create node add data
		if(size == 0) {
			head = newNode;
			tail = newNode; //update tail ref
			size++;
			return;
		}
		tail.next = newNode; // make tail point to new node
		tail = newNode; //updating our tail ref
		size++;


	}

	public void add(int index , KEY d) {
		//4 cases: add head,tail,first node,middle
		if(index  > size || index < 0) {
			System.out.println("Invalid input");
			return ;
		}


		//add tail
		if(index == size) {
			add(d);
		}
		//add first node to list
		else if(size == 0) {
			add(d);

		}
		else if(index == 0) {
			Node<KEY> newHead = new Node<KEY>(d,head);
			head = newHead;
			size++;
		}
		else {
			Node<KEY> currentLocation,previousLocation;
			currentLocation = head.next;
			previousLocation = head;
			int counter = 1;
			while(counter < index) {
				currentLocation = currentLocation.next;
				previousLocation = previousLocation.next;
				counter++;
			}
			Node<KEY> newNode = new Node<KEY>(d, currentLocation);
			previousLocation.next = newNode;
			size++;

		}



	}

	public KEY get(int index) {
		Node<KEY> currentPosition = head;
		int counter = 0;

		//check to make sure index is less than size

		if(index  >= size || index < 0) {
			System.out.println("Invalid input");
			return null;
		}

		while(counter < index) { 
			currentPosition = currentPosition.next;
			counter++;
		}

		return currentPosition.data; 

	}

	public void remove(int index) {

		//4 cases : removing the head, removing the tail, removing single node, regular

		if(index  >= size || index < 0) {
			System.out.println("Invalid input");
			return ;
		}


		if (index == 0) {
			head = head.next;

			if(size == 1) {
				tail = null;
			}
		}
		else {
			Node<KEY> currentLocation,previousLocation;
			currentLocation = head.next;
			previousLocation = head;
			int counter = 1;
			while(counter < index) {
				currentLocation = currentLocation.next;
				previousLocation = previousLocation.next;
				counter++;
			}

			previousLocation.next = currentLocation.next;

			if(index == size - 1) {
				tail = previousLocation;
			}


		}
		size--;
	}


	public int size() {
		return size;
	}
}


