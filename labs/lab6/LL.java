public class LL {

	private class Node {
		protected Node next;
		protected int data;

		public Node() {
			next = null;
			data = 0;
		} //Node (constructor)

		public Node(int d, Node n) {
			next = n;
			data = d;
		} //Node (constructor)

	} //Node (class)


	// LL stuff starts here
	private int size;
	private Node head, tail;

	public LL() {
		head = null;
		tail = null;
		size = 0;
	} //LL (constructor)

	public void add(int d) {
		Node newNode = new Node(d, null); // create Node, add data

		if (size == 0) {
			head = newNode;
			tail = newNode; //update tail reference
			size++; //increment size
			return;
		} //if
		
		tail.next = newNode; // make tail point to new node
		tail = newNode; // update tail reference
		size++;

	} //add

	public void add(int index, int d) {

	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		while (counter < index) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		// check to make sure the index is less than size

		return currentPosition.data; 
	} //get

	public void remove(int index) {

	} //remove

	public int size() {
		return size;
	} //size

} //LL (class)
