public class ConverterMain {
	public static void main(String [] args) {
		char [] infix = args[0].toCharArray();
		Converter c = new Converter(infix);
		c.infixToPostFix();
		c.display();
	}
}