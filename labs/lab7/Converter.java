import java.util.Stack;

public class Converter {
	char [] infix;
	Stack<Character> postfix;
	StringBuilder pf;
	public Converter(char [] s) {
		infix = s;
		postfix = new Stack<Character>();
		pf = new StringBuilder();
	}
	public void infixToPostFix() {
		Parenthesis p = new Parenthesis();
		if(p.matcher(infix) == true) {
			for(int i =0;i<infix.length;i++) {
				check(infix[i],postfix);
			}
			while(!postfix.empty())
				pf.append(postfix.pop());
		}
		else {
			System.out.println("Check parenthesis!");
			return ;
		}
	}
	//TODO:Take care of character case
	public void check(char next,Stack<Character> s) {
		if(next >= 'a' && next <= 'z')
			pf.append(next);
		else if(s.empty())
			s.push(next);
		else if(s.peek() == '(') {
			if(next == '(')
				s.push(next);
			else if(next == ')') {
				while(s.peek()!='(')
					pf.append(s.pop());
				s.pop();
			}
			else if(next == '+'||next=='-'||next=='*'||next=='/')
				s.push(next);
		}
		else if(s.peek()=='+'||s.peek()=='-') {
			if(next=='(')
				s.push(next);
			else if(next==')') {
				while(s.peek()!='(')
					pf.append(s.pop());
				s.pop();
			}
			else if(next == '+'||next=='-') {
				pf.append(s.pop());
				s.push(next);
			}
			else if(next == '*' || next =='/') {
				s.push(next);
			}
		}
		else if(s.peek() == '*' || s.peek() == '/') {
			if(next=='(')
				s.push(next);
			else if(next==')') {
				while(s.peek()!='(')
					pf.append(s.pop());
				s.pop();
			}
			else if(next == '+'||next=='-'||next == '*'||next == '/') {
				pf.append(s.pop());
				s.push(next);
			}
		}

	}
	public void display() {
		System.out.println(pf);
	}
}