public class Hashtable {
	private String [] keys;
	private String [] values;

	private final int SIZE = 11;
	
	public Hashtable() {
		keys = new String[SIZE];
		values = new String[SIZE];
		for(int i =0;i<SIZE;i++) {
			keys[i] = "null";
			values[i] = "null";
		}
	}

	public void put(String key,String value) {
		int hashValue = hash(key);
		int counter = 1;
		if(!keys[hashValue].equals("null")) {
			while(counter < SIZE) {
				if(keys[(hashValue + counter)%11].equals("null")) {
					keys[(hashValue + counter)%11] = key;
					values[(hashValue + counter)%11] = value;
					break;
				}
				counter++;
			}
			if(counter == SIZE) {
				System.out.println("no more space!");
				//return;
			}
		}
		else {
			keys[hashValue] = key;
			values[hashValue] = value;
		}
	}

	public String get(String key) {
		int hashValue = hash(key);
		String getValue = keys[hashValue];
		int counter = 1;
		String value = new String();
		if(!key.equals(getValue)) {
			while(counter < SIZE) {
				getValue = keys[(hashValue + counter)%11];
				if(key.equals(getValue)){
					value = values[(hashValue + counter)%11];
					break;
				}
				counter++;
			}
			if(counter == SIZE) {
				System.out.println("error! key not found");
				value = "null";
			}
		} else {
			value = values[hashValue];
		}
		return value;
	}

	private int hash(String key) {
		char [] keyChar = key.toCharArray();
		int asciiSum = 0;
		for(int i = 0;i<keyChar.length;i++) 
			asciiSum+=(int)keyChar[i];
		return asciiSum%11;
	}

	public void printAll() {
		for(int i =0;i<SIZE;i++) {
			if(!keys[i].equals("null")) {
				System.out.println(keys[i]+" - "+values[i]);
			}
		}
	}

}
