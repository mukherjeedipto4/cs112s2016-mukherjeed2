import java.io.*;
import java.util.Scanner;
public class AuthenticationSystem {
	public static void main(String [] args) throws FileNotFoundException{
		Hashtable h = new Hashtable();
		readFromFile(h);
		/*System.out.println("Printing the username/password combinations");
		h.printAll();*/
		Scanner userInput = new Scanner(System.in);
		String userName;
		String password;
		String hashtableValue;
		int counter = 0;
		while(counter < 5) {
			System.out.println("Enter a user name : ");
			userName = userInput.next();
			System.out.println("Enter the password : ");
			password= userInput.next();
			hashtableValue = h.get(userName);
			if(hashtableValue.equals("null")) {
				System.out.println("AUTHENTICATION FAILED");
				System.out.println("Username/password combination does not exist");
				counter++;
			}
			else if(!hashtableValue.equals(password)) {
				System.out.println("AUTHENTICATION FAILED");
				System.out.println("Incorrect password");
				counter++;
			}
			else {
				System.out.println("AUTHENTICATION SUCCESS!");
				break;
			}

		}

	}

	public static void readFromFile(Hashtable h) {
		try {
			File file = new File("passwd.txt");
			Scanner scanFile = new Scanner(file);
			String line;
			scanFile.useDelimiter("\n");
			while(scanFile.hasNext()) {
				line = scanFile.next();
				Scanner scanLine = new Scanner(line);
				scanLine.useDelimiter(" ");
				String userName = scanLine.next();
				String passwd = scanLine.next();
				h.put(userName,passwd);
			}
		} catch(FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		}
	} 
}