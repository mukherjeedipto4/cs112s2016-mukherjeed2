public class Oval extends Round {

	private double major,minor;

	public Oval(double maj,double min) {
		super();
		major = maj;
		minor = min;
	}

	public void calculateArea() {
		area = PI*major*minor;
	}

	public void calculatePerimeter() {
		perimeter = 2*PI*Math.sqrt((major*major + minor*minor)/2);
	}

}

