public class Circle extends Round {

	private double radius;

	public Circle(double r) {
		super();
		radius = r;
	}

	public void calculateArea() {
		area = PI*radius*radius;
	}

	public void calculatePerimeter() {
		perimeter = 2*PI*radius;
	}

}

