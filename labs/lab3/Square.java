public class Square extends Rect {

	public Square(double side) {
		//squares have the same height and width
		super(side,side);
	}

	public void calculateArea() {
		area = height * width;
	}

	public void calculatePerimeter() {
		perimeter = 4 * width;
	}

}
