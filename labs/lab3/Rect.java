public class Rect extends Quad {
	
	public Rect(double w,double h) {
		width = w;
		height = h;

	}

	public void calculateArea() {
		area = width*height;
	}

	public void calculatePerimeter() {
		perimeter = 2*(width + height);
	}

}

