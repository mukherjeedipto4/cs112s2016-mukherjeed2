public class Line extends Shape {

	private double x1,x2,y1,y2;

	public Line(double x_one,double y_one, double x_two, double y_two) {
		x1 = x_one;
		y1 = y_one;
		x2 = x_two;
		y2 = y_two;
	}


	public void calculateArea() {
		//a line encloses zero area
		area = 0;
	}

	public void calculatePerimeter() {
		perimeter = 2*Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
	}
}
