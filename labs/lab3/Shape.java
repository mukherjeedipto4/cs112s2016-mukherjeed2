public class Shape {

	protected double area;
	protected double perimeter;

	public Shape() {
		area = 0;
		perimeter = 0;
	}

	public double getArea() {
		return area;
	}

	public double getPerimeter() {
		return perimeter;
	}

}


	



