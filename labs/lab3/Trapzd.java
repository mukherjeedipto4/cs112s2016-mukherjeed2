public class Trapzd extends Quad {
	private double otherBase;
	private double side1;
	private double side2;

	public Trapzd(double base1,double h,double base2,double s1,double s2) {
		//width defined in quad class is used as lower base
		//height is used as the height between two parallel bases
		width = base1;
		height = h;
		otherBase = base2;
		side1 = s1;
		side2 = s2;
	}

	public void calculateArea() {
		area = height*((width+otherBase)/2);
	}

	public void calculatePerimeter() {
		perimeter = width + otherBase + side1 + side2;
	}

}

