//Program to print "Hooray!" infinite times using a while loop.
public class Hooray { 						//A class Hooray is created

    public static void hooray() { 			//function that contains the loop

		while(true) { 						//condition is always true
			System.out.println("Hooray!"); 	//infinite while loop
    	} 

    } 

    public static void main(String[] args) { //the main function which calls hooray()
		hooray(); 							//function called
    } 

} 
