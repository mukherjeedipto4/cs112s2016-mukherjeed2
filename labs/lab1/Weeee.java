//Program to print "Weeee!" infinite times by using recursion until the program runs out of memory
public class Weeee { 						//A class Weeee

    public static void weeee() { 			//recursive function that prints "Weeee!"
		System.out.println("Weeee!"); 		//prints "Weeee!"
		weeee(); 							//recursive function calling
    }

    public static void main(String[] args) {
		weeee(); 							//function is called
    }

}
