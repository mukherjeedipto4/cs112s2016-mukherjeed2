class DoublyLinkedList {

	private static class Node {

		protected int data;
		protected Node next;
		protected Node previous;

		public Node() {
			next = null;
			previous = null;
			data = 0;
		} //Node (constructor)

		public Node(int d, Node n,Node p) {
			next = n;
			data = d;
			previous = p;
		} //Node (constructor)

	} //Node (class)



    // DoublyLinkedList stuff starts here
	private int size;
	private Node head, tail;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

	public void add(int newInt) {
		//CASE 1 : Inserting a new node to a new linked list
		//CASE 2 : Inserting a new node at the end of the list

		if(size == 0) {
			Node newNode = new Node(newInt,null,null);
			head = newNode;
			tail = newNode;
		}
		else {
			Node newNode = new Node(newInt,null,tail);
			tail.next = newNode;
			tail = newNode;

		}
		size++;		
	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {
		Node currentPosition = tail;
		int counter  = 0;
		while(counter < index) {
			currentPosition = currentPosition.previous;
			counter++;
		}
		return currentPosition.data;
	} //getFromEnd

	public void remove(int index) {
		//Case 1 : removing the head
		//case 2 : removing the tail
		//case 3: removing the only node
		//case 4 : removing from the middle

		if(index == 0 && size == 1) {
				head = null;
				tail = null;
		}
		else if(index == 0) {
				head = head.next;
				head.previous = null;
		}
		else if(index == size-1) {
			tail = tail.previous;
			tail.next = null;

		}
		else {
			Node previousNode = head;
			Node currentNode = head.next;
			int counter = 1;
			while(counter<index) {
				previousNode = previousNode.next;
				currentNode = currentNode.next;
				counter++;
			}
			previousNode.next = currentNode.next;
			currentNode = currentNode.next;
			currentNode.previous = previousNode;
		}
		size--;
	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) {
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
