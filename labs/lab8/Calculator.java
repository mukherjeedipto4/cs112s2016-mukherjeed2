import java.util.Stack;
public class Calculator {
	Stack<Integer> s;
	//Converter infixToPostfix;

	public Calculator() {
		s = new Stack<Integer>();
		//infixToPostfix = new Converter();
	}

	public void calculate(char [] postfix) {
		int result;
		for(int i =0;i<postfix.length;i++) {
			if(postfix[i]>= '0' && postfix[i]<='9') {
				s.push(Character.getNumericValue(postfix[i]));
			}
			else {
				int temp_result, firstNum, secondNum;
				secondNum = s.pop();
				firstNum = s.pop();
				switch(postfix[i]) {
					case '+' :
						temp_result = firstNum + secondNum;
						s.push(temp_result);
						break;
					case '-' :
						temp_result = firstNum - secondNum;
						s.push(temp_result);
						break;
					case '*' :
						temp_result = firstNum * secondNum;
						s.push(temp_result);
						break;
					case '/' :
						temp_result = firstNum / secondNum;
						s.push(temp_result);
						break;
					default :
						System.out.println("Unhandled exception");
						break;
				}

			}	
		}
		result = s.pop();
		System.out.println("The result is : " + result);

	}

}