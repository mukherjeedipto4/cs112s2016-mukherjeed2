public class CalculatorMain {
	public static void main(String [] args) {
		char [] userInput = args[0].toCharArray();
		Converter convert = new Converter(userInput);
		Calculator calc = new Calculator();
		convert.infixToPostFix();
		calc.calculate(convert.getPostFix());
	}
}