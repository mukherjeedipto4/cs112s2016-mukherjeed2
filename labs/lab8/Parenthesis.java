import java.util.Stack;
class Parenthesis {

	public boolean matcher(char [] c) {
		Stack<Character> myStack = new Stack<Character>();
		for(int i =0;i<c.length;i++) {
			if(c[i] == '(' || c[i] == '[' || c[i] == '{') {
				myStack.push(c[i]);
			}
			else if(!myStack.empty()) {
				if(c[i] == ')'){
					char temp = myStack.pop();
					if(temp != '(' ) {
						return false;
					}
				}
				else if(c[i] == ']') {
					char temp = myStack.pop();
					if(temp != '[') {
						return false;
					}

				}
				else if(c[i] == '}') {
					char temp = myStack.pop();
					if(temp != '{') {
						return false;
					}

				}
			} else if(myStack.empty() && (c[i] ==')' || c[i] == '}' || c[i] == ']')){
				return false;
			}
				
		}
		if(myStack.size() != 0) {
			return false;
		} else {
			return true;
		}
	}
}
