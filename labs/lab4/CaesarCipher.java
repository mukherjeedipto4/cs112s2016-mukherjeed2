import java.util.Scanner;
public class CaesarCipher {
	//actual offset = offset % 26 as offset of 26 is 0
	static int actualOffset;
	public static void main(String [] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a line of text : ");
		int offset = 3;
		actualOffset = (offset%26);
		String text = scan.nextLine();
		//converted the text to uppercase for an easier implementation
		text = text.toUpperCase();
		char [] textArray = text.toCharArray();	
		encrypt(textArray);
		System.out.println(textArray);
		
		decrypt(textArray);
		System.out.println(textArray);

	}

	public static void encrypt(char [] plainText) {
		//A = 65 , Z = 90
		//If the character lies between 65 and 90-actual offset, we add the actual offset to the character
		//If character + actualOffset > 90 , we have to wrap around
		for(int i=0;i<plainText.length;i++) {
			if((int)plainText[i]>=65 && (int)plainText[i] <=(90-actualOffset)) {
				plainText[i] = (char)((int)plainText[i]+actualOffset);
			}
			else if((int)plainText[i] > (90-actualOffset) && (int)plainText[i] <=90) {
				plainText[i] = (char)(64 + ((int)plainText[i]+actualOffset - 90));
			}
			
		}

	}

	public static void decrypt(char [] cipherText) {
		//If the character lies between 65+actualOffset(inclusive) and 90, we subtract the actualOffset
		//to get the original character.
		//If the character lies between 65(inclusive) and 65+actualOffset(exclusive) we need to wrap around
		for(int i =0;i<cipherText.length;i++) {
			if((int)cipherText[i]>=(65+actualOffset) && (int)cipherText[i]<=90) {
				cipherText[i] = (char)((int)cipherText[i] - actualOffset);
			}
			else if((int)cipherText[i] >= 65 && (int)cipherText[i]<(65+actualOffset)) {
				cipherText[i] = (char)(90 - (actualOffset + 65 - (int)cipherText[i]) + 1);
			}
		}

	}
}



