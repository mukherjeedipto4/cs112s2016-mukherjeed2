public class SelectionSort {
	public static void main(String [] args) {
		int [] myArray = {8,7,1,6,3};
		int currentMin;
		int currentMinPos = 0;
		displayArray(myArray);
		//print the current unsorted array
		System.out.println();
		for(int i=0;i<(myArray.length)-1;i++) {
			currentMin = myArray[i];
			for(int j=i;j<myArray.length;j++) {
				if(myArray[j] < currentMin) {
					currentMin = myArray[j];
					currentMinPos = j;
				}
			}
			//swap the minimum of the current array with the current position
			int temp = myArray[i];
			myArray[i] = myArray[currentMinPos];
			myArray[currentMinPos] = temp;
			displayArray(myArray);
			System.out.println();
		}
	}
	public static void displayArray(int [] a) {
		for(int i =0;i<a.length;i++) {
			System.out.print(a[i]+" ");
		}
	}
}


