//Program that allows the computer to play the guessing game
//CMPSC112S2016
//Diptajyoti Mukherjee
//Lab # 02
//Honor Code : This work is mine unless cited. 
import java.util.Scanner;

public class Lab2Part2 {
	static int low,high,counter;
	static Scanner scan = new Scanner(System.in);
	public static void main(String [] args) {
		String playAgain;
		
		do {
			low = 1;
			high = 100;
			counter = 0;
			System.out.println("Please pick a number between 1 and 100");
			computerPlay();

			System.out.println("Do you want to play again? (yes/no)");
			playAgain = scan.next();


		}while(playAgain.equalsIgnoreCase("yes"));

	}

	public static void computerPlay() {
		boolean next = false;
		do {
			int computerGuess = (high+low)/2;
			counter++;
			System.out.println("My guess was : " + computerGuess);
			System.out.println("How did I do?");
			int guess = scan.nextInt();
			//1 if computer guess is too high
			//-1 if computer guess is too low
			//0 if computer guess is equal to the number guessed
			if(guess == 1) {
				high = computerGuess -1;
				next = true;
			}
			else if(guess == -1) {
				low = computerGuess + 1;
				next = true;
			}
			else {
				System.out.println("Yay! I got it in "+counter+" tries");
				System.out.println("My number is : " + computerGuess);
				next = false;
			}
		}while(next);
	}

}




