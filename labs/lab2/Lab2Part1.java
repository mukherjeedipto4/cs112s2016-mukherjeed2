//Program that allows user to guess a randomly selected number
//CMPSC112S2016
//Diptajyoti Mukherjee
//Lab # 02
//Honor Code : This work is mine unless cited. 

import java.util.Scanner;
import java.util.Random;

public class Lab2Part1 {
	public static void main(String [] args) {
		Random rand = new Random();
		int guess;
		//true if guess is not correct
		//false if guess correct
		boolean next = true; 		
		Scanner scan = new Scanner(System.in);
		String playAgain;
		do {
			int computerNumber = rand.nextInt(100)+1;
			int counter = 0;

			do {
				System.out.println("Please enter your guess :");
				guess = scan.nextInt();
				counter++ ;
				next = guessChecker(guess,computerNumber);

			}while(next);
			System.out.println("You got it in " + counter +" tries");

			System.out.println("Do you want to play again?(yes/no)");
			playAgain = scan.next();
		}while(playAgain.equalsIgnoreCase("yes"));
	}
	public static boolean guessChecker(int g, int cN) {
		//returns true if guess is too high / low
		//returns false if guess is correct
		if(g > cN) {
			System.out.println("Your guess was too high!");
			return true;
		}
		else if(g < cN) {
			System.out.println("Your guess was too low!");
			return true;
		}
		else {
			System.out.println("Your guess was correct!");
			System.out.println("My number was : " + cN);
			return false;
		}
	}
}

