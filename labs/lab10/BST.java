public class BST {
	private class Node {
		protected int data;
		protected Node leftChild;
		protected Node rightChild;
		protected Node parent;

		public Node() {
			data = 0;
			leftChild = null;
			rightChild = null;
			parent = null;
		}
		public Node(int d) {
			data = d;
		}  
	}
	private Node root;
	private int size;

	public BST() {
		root = new Node();
		size = 0;
	}
	public int get(int n) {
		Node currentNode = root;
		int value;
		while(currentNode.leftChild!=null || currentNode.rightChild!=null) {
			if(n < currentNode.data && (currentNode.leftChild!=null)) {
				currentNode = currentNode.leftChild;
			}
			else if(n > currentNode.data &&(currentNode.rightChild!=null)) {
				currentNode = currentNode.rightChild;
			} else {
				break;
			}
		}
		value = currentNode.data;
		return value;
	}
	public void insert(int toBeInserted) {
		if(size == 0) {
			root.data = toBeInserted;
			size++;
		} else {
			Node newNode = new Node(toBeInserted);
			Node currentNode = root;
			while(true) {
				if(toBeInserted < currentNode.data) {
					//System.out.print(currentNode.data+"-");
					if(currentNode.leftChild == null) {
						currentNode.leftChild = newNode;
						newNode.parent = currentNode;
						size++;
						break;
					} else {
						currentNode = currentNode.leftChild;
					}
				}
				if(toBeInserted > currentNode.data) {
					//System.out.print(currentNode.data+"-");
					if(currentNode.rightChild == null) {
						currentNode.rightChild = newNode;
						newNode.parent = currentNode;
						size++;
						break;
					} else {
						currentNode = currentNode.rightChild;
					}
				}
			}
		}
	}
	public void remove(int n) {
		Node currentNode = root;
		while(currentNode.leftChild!=null || currentNode.rightChild!=null) {
			if(n < currentNode.data && currentNode.leftChild!=null) {
				currentNode = currentNode.leftChild;
			}
			else if(n > currentNode.data &&currentNode.rightChild!=null) {
				currentNode = currentNode.rightChild;
			} else {
				break;
			}
		}
		Node parentNode = currentNode.parent;
		if(currentNode.leftChild == null && currentNode.rightChild!= null) {
			parentNode.rightChild = currentNode.rightChild;
			currentNode.rightChild.parent = parentNode;
		} else if(currentNode.leftChild != null && currentNode.rightChild == null) {
			parentNode.leftChild = currentNode.leftChild;
			currentNode.leftChild.parent = parentNode;
		} else if(currentNode.leftChild == null && currentNode.rightChild == null) {
			parentNode.leftChild = null;
			currentNode.parent = null;
		}
	}
}